import { defineCollection, z } from 'astro:content'

const posts = defineCollection({
	// Type-check frontmatter using a schema
	schema: z.object({
		title: z.string(),
		description: z.string(),
		// Transform string to Date object
		pubDate: z
			.string()
			.optional()
			.or(z.date())
			.transform((str) => (str ? new Date(str) : undefined)),
		updatedDate: z
			.string()
			.optional()
			.or(z.date())
			.transform((str) => (str ? new Date(str) : undefined)),
		tags: z.array(z.string()).optional().default([]),
		lang: z.string().optional().default('en'),
		comment: z.boolean().optional().default(true),
	}),
})

const notes = defineCollection({
	schema: z.object({
		title: z.string(),
		pubDate: z
			.string()
			.optional()
			.or(z.date())
			.transform((str) => (str ? new Date(str) : undefined)),
		updatedDate: z
			.string()
			.optional()
			.or(z.date())
			.transform((str) => (str ? new Date(str) : undefined)),
		lang: z.string().optional().default('en'),
		comment: z.boolean().optional().default(false),
	}),
})

export const collections = { posts, notes }
