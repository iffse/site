---
title: 1D Wave Equations
pubDate: 2025-01-16
---

By ignoring viscosity and heat conduction, which are less important in numerical computations, the resulting equations are the Euler equations which are based in Navier-Stokes equations in an ideal flow.

As numerical schemes should behave correctly with small perturbations, instead of working with a generic flow $\mathbf U$, we will be working with a perturbation $\mathbf u'$ of a base flow $\mathbf U_0$ that is stationary and homogeneous. Therefore:
$$
\mathbf U=\mathbf U_0+\mathbf u'
$$
Then, if the scheme works for $\mathbf u'$, it will work for a generic flow $\mathbf U$.

The 1D linearized Euler equations can be de split into 3 equations of characteristic variables with the following form:
$$
\frac{\partial u}{\partial t}+c\frac{\partial u}{\partial x}=0
$$

## Contents

## Analysis

Linear wave equation with constant $c>0$ (propagation speed):
$$
\frac{\partial u}{\partial t}+c\frac{\partial u}{\partial x}=0
$$
Boundary conditions:

- Non-periodic (general): $u_0=u_0(t)$
- Periodic: $u(0)=u(L)$

For periodic boundary conditions, solutions are plain waves.[^plain-waves] Using Fourier analysis:
$$
u=\sum_{k=-\infty}^\infty\hat u_k(t)e^{\iu kx}
$$

- $\iu=\sqrt{-1}$
- $k=2\pi/l$: Number of waves, with $l$ the length of the wave

[^plain-waves]: Von Neumann analysis of wave equation

Hypothesis:

- Variable separation: Solution can be split into the product of a spatial and a temporal part
- Linearity: Superposition, $u=\hat u_k(t)e^{\iu kx}+\text{conditions}$

Note that $\hat u_k(t)e^{\iu kx}$ is the boundary condition of $\hat u_{-k}(t)e^{-\iu kx}$. Therefore, the wave equation:
$$
\cancel{e^{\iu kx}}\frac{\dd\hat u_k(t)}{\dd t}=-\iu ck\hat u_k(t)\cancel{e^{\iu kx}}
$$
Which results into an initial value problem:
$$
\boxed{\frac{\dd \hat u_k(t)}{\dd t}=\lambda\hat u_k(t)\quad;\quad\lambda=-\iu ck\quad;\quad}
$$

- $\lambda$: Eigenvalue of the Jacobian when linearized
- $\hat u_k$: Fourier mode
- $\hat u_k(t=0)=\hat u_{k,0}$: Initial condition

### Integrations

The equation can be easily integrated, obtaining:
$$
\hat u_k(t)=\hat u_k(t=0)e^{\lambda t}=\hat u_k(t=0)e^{-\iu ckt}
$$

The **analytical** amplification factor (gain) $G$ is defined as:
$$
\boxed{G=\frac{u(t+\Delta t)}{u(t)}= \left| \frac{u(t+\Delta t)}{u(t)} \right|e^{\iu\Phi_a}\\}
$$

- $u=\hat u_k$ here, as the difference will cancel out
- $\Phi_a$: Analytical phase

When using **numerical** integration for the time:
$$
\boxed{G=\frac{u^{n+1}}{u_n}=\left| \frac{u^{n+1}}{u^n} \right|e^{\iu\Phi_n}}
$$

Using solutions like  $\hat u_k(t)=\hat u_k(t=0)e^{\lambda t}$ in **analytical integration**:
$$
\boxed{G=\frac{e^{\lambda(t+\Delta t)}}{e^{\lambda t}}=e^{\lambda\Delta t}}
$$

- In general, $\lambda=\lambda_R+\iu\lambda_I$, therefore $G=e^{\lambda_R\Delta t}e^{\iu\lambda_I\delta t}$
- The modulus is therefore $|G|=e^{\lambda_R\Delta t}$
- Integration is **stable when** $|G|\le1$

### Computational Variables

- $\hat k=k\Delta x$: Non-dimensional wave number
	- $\hat k=2\pi\Delta x/l=2\pi/N_{\text{points per wave lengh}}$
	- $\hat k\rightarrow0$: Low frequency waves. Correctly computed
	- $\hat k\rightarrow\pi$: high frequency waves. Wrong computation ($G>1$)
	- Refining effect: Keeping $k$ constant, finer discretizations gives better results: $\Delta x\propto\hat k$
- $\mathrm{CFL}=c\Delta t/\Delta x$: Courant-Friedrich-Lewy number

$$
\boxed{\lambda\Delta t=-\iu ck\Delta t=-\iu\mathrm{CFL}\hat k}
$$

### Dispersion Relation

In some problems, perturbations depend on time harmonically : $\hat u_k(t)=\hat u_k^0e^{\iu\omega t}$.

- $\omega=2\pi/T$: Angular frequency
- $T$: Temporal period of the perturbation

Introducing this solution into PVI:
$$
\frac{\dd\hat u_k(t)}{\dd t}=\lambda\hat u_k(t)\quad \rightarrow \quad \iu\omega\cancel{u_k^0e^{\iu\omega t}}=\lambda\cancel{u_k^0e^{\iu\omega t}} \quad\rightarrow\quad \iu\omega-\lambda=0
$$
With $\lambda=-\iu ck$, then the **dispersion relation** is:
$$
\iu\omega+\iu ck=0\quad \rightarrow \quad\omega+ck=0
$$

- For a harmonic perturbation $\omega$, as $c$ is constant, the wave is excited with $k=-\omega/c$

### Linear Wave Equation with Dissipation

The equation:
$$
\frac{\partial u}{\partial t}+c\frac{\partial u}{\partial x}=\nu\frac{\partial^2u}{\partial x^2}
$$

- Initial conditions: $u(x,0)=u_I(x)$
- Contour conditions:
	- $u(0,t)=u_0(t)$ for all $x$
	- $u(L,t)=u_L(t)$ for all $t$

Supposing solutions like $u(x,t)=\hat u_k(t)e^{\iu kx}$ and:
$$
\frac{\dd\hat u_k}{\dd t}=\lambda\hat u_k(t)
$$
Where $\lambda=-\iu ck-\nu k^2$. As $\lambda_R=-\nu k^2$ results in $|G|<1$, the solution is damped by viscosity.

Integrating the equation gives:
$$
\hat u_k(t)=\hat u_k(t=0)e^{\lambda t}=\hat u_k(t=0)e^{(-\iu ck-\nu k^2)t}
$$
As $u(x,t)=\hat u_k(t)e^{\iu kx}$ and $\hat u_k(t)=\hat u_k(t=0)e^{\lambda t}$:
$$
u(x,t)=\hat u_k(t=0)e^{\iu k(x-\iu\lambda t/k)}
$$
Separating by parts:
$$
u(x,t)=\hat u_k(t=0)e^{\iu k(x+\lambda_I t/k)}e^{\lambda_Rt}
$$
In this case, the dispersion relation is:
$$
\iu\omega+\iu ck+\nu k^2=0
$$

## Numerical Resolution

Method of lines for a generic internal node $j$:
$$
\frac{\dd u_j}{\dd t}=-c \left. \frac{\partial u}{\partial x} \right|_j
$$

### Central Discretization without Viscosity

Given the central discretization:
$$
\frac{\dd u_j}{\dd t}=-\frac{c}{2\Delta x}(u_{j+1}-u_{j-1})
$$
Using Fourier:

- $u_j=\hat u_k(t)e^{\iu kx_j}$
- $u_{j-1}=\hat u_k(t)e^{\iu kx_{j-1}}=\hat u_k(t)e^{\iu kx_j-\iu k\Delta x}=\hat u_k(t)e^{\iu kx_j}e^{-\iu\hat k}$
- $u_{j+1}=\hat u_k(t)e^{\iu kx_{j+1}}=\hat u_k(t)e^{\iu kx_j+\iu k\Delta x}=\hat u_k(t)e^{\iu kx_j}e^{\iu\hat k}$

Injecting into the previous equation and cancel out $e^{\iu kx_j}$:
$$
\frac{\dd \hat u_k}{\dd t}=-\frac{c}{2\Delta x} \left( e^{\iu\hat k}-e^{-\iu\hat k} \right)\hat u_k
$$
As $e^{\iu\hat k}-e^{\iu\hat k}=2\iu\sin(\hat k)$:
$$
\frac{\dd\hat u_k}{\dd t}=-\iu\frac{c}{\Delta x}\sin(\hat k)\hat u_k=\lambda\hat u_k
$$

The new eigenvalue $\lambda=-\iu c\sin(\hat k)/\Delta x$ is the same as analytical value for $\hat k\ll1$, not for $\hat k=\pi$, which are not damped.

The phase velocity $v_f=-\lambda_I/k$ is then:
$$
v_f=\frac{c\sin(\hat k)}{\hat k}
\begin{cases}
c&\text{if }\hat k\ll1\\
0&\text{if }\hat k=\pi
\end{cases}
$$
High frequency waves are therefore, wrongly computed.

### Central Discretization with Artificial Viscosity

We will be using artificial viscosity to damp high frequency waves, keeping the low frequency waves intact:
$$
\frac{\partial u}{\partial t}+c\frac{\partial u}{\partial x}=\mu_x|c|\Delta x\frac{\partial^2 u}{\partial x^2}
$$
The above equations results in, after using method of lines:
$$
\frac{\dd\hat u_k}{\dd t}=
\left[ -\iu\frac{c}{\Delta x}\sin(\hat k)-\frac{2\mu_2c}{\Delta x}(1-\cos(\hat k)) \right]\hat u_k=\lambda\hat u_k
$$
That is:
$$
\boxed{\Delta t\lambda=-\iu\mathrm{CFL}\sin(\hat k)-2\mu_2\mathrm{CFL}(1-\cos(\hat k))}
$$
Where the maximum damping is at $\hat k=\pi$.
