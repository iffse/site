---
title: Irrotational Movements
pubDate: 2024-08-01
---

## Contents

## Coordinate System and Hypothesis

The coordinate system used has the $x$ component in the direction of the flight velocity[^flightvel], and $z$ to the up. Force in $x$ is drag, and in $z$ is lift.

[^flightvel]: Pointing towards the tail of the aircraft. Also knows as the wind's system.

Considerations:

- Negligible viscosity, except in zones such as boundary layer and wake
	- $\mathrm{Re}=\rho_\infty U_\infty c/\mu\approx 4\times10^6$ for conventional flight. The
- Adiabatic movement
	- The heat conduction negligible with $\mathrm{RePr}\gg1$, given $\mathrm{Re}\gg1$
- Negligible static forces such as gravity given $gL\ll V^2$

## Equations of Movements

Differential equations in partial derivatives for the movement of the fluid, given negligible viscosity and heat conduction

**Continuity**:
$$
\frac{\partial \rho}{\partial t}+\nabla\cdot(\rho\mathbf V)=\frac{\DD \rho}{\DD t}+\rho\nabla\cdot\mathbf V=0
$$

**Conservation of momentum**:
$$
\frac{\DD\mathbf V}{\DD t}=\frac{\partial\mathbf V}{\partial t}+\mathbf V\cdot\nabla\mathbf V=\frac{\partial\mathbf V}{\partial t}+\nabla\left( \frac{V^2}{2} \right)-\mathbf V\times(\nabla\times\mathbf V)=-\frac{1}{\rho}\nabla p
$$
with $V=|\mathbf V|$.

**Evolution of entropy**:
$$
\frac{\DD s}{\DD t}=\frac{\partial s}{\partial t}+\mathbf V\cdot\nabla s=0
$$
in perfect gas model, this equation can be substituted with $p/\rho^\gamma=\text{const.}$

## Euler-Bernoulli Equation

If the vorticity conserves and zero $\nabla\times\mathbf V=0$ the velocity can be represented as the **potential of velocities** $\Phi=\Phi(x,y,z,t)$, such that $\mathbf V=\nabla\Phi$.

Applying this to the equation of conservation of momentum, we obtain the equation of **Euler-Bernoulli**:
$$
\boxed{\frac{\partial \Phi}{\partial t}+\frac{|\nabla\Phi|^2}{2}+\int\frac{\dd p}{\rho}=F(t)}
$$

For **liquids**:
$$
\rho\frac{\partial \Phi}{\partial t}+\rho\frac{V^2}{2}+p=G(t)
$$

For **perfect gas**, with the velocity of the sound $a^2=\gamma RT$:
$$
\frac{\partial \Phi}{\partial t}+\frac{V^2}{2}+\frac{a^2}{\gamma-1}=G(t)
$$
