---
title: New start
description: A brand new web page!
tags: [misc]
pubDate: '2023-12-06'
---

A brand-new web page, written with Astro and the color palette from Catppuccin theme!

More features are coming soon, such as dark mode, tags, an archive of all pages, more directories to group related pages together (for literatures writings, for example!), etc.

I am not sure how Astro behaves when the project gets larger, but so far it looks very promising. It's much more flexible than a static site generator while still being able to generate static sites without JS.

