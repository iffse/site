import { defineConfig } from 'astro/config'
import mdx from '@astrojs/mdx'
import remarkToc from 'remark-toc'
import remarkMath from 'remark-math'
import remarkCaptions from 'remark-captions'
import rehypeKatex from 'rehype-katex'
import sitemap from '@astrojs/sitemap'

// https://astro.build/config
export default defineConfig({
	site: 'https://iffse.eu.org',
	integrations: [mdx(), sitemap()],
	redirects: {
		'/post': '/posts/1',
		'/posts': '/posts/1',
		'/tag': '/tags',
		'/note': '/notes',
		'/subject': '/notes',
		'/stripe': 'https://donate.stripe.com/00g8zCdSu0Xr7TO4gg',
	},
	markdown: {
		shikiConfig: {
			theme: 'material-theme-lighter',
			langs: [],
			wrap: true,
		},
		remarkPlugins: [remarkToc, remarkMath, remarkCaptions],
		rehypePlugins: [
			[
				rehypeKatex,
				{
					macros: {
						'\\sech': '\\operatorname{sech}',
						'\\dd': '\\mathop{}\\!\\mathrm{d}',
						'\\DD': '\\mathop{}\\!\\mathrm{D}',
						'\\xint': '\\times\\mkern-20mu\\int',
						'\\celsius': '\\degree\\mathrm{C}',
						'\\mathbf': '\\pmb{\\mathrm{#1}}',
						'\\iu': '\\mathrm{i}',
					},
				},
			],
		],
	},
})
